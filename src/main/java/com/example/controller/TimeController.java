package com.example.controller;

import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TimeController {

	@GetMapping("/time")
	public LocalDateTime timeAndHour() {
		return LocalDateTime.now();
	}
}

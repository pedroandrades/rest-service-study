package com.example.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class CEPController {
	
	@GetMapping("/cep")
	public ResponseEntity<String> viaCep(@RequestParam(name="numero", required=false) String numero) {
		String url = String.format("https://viacep.com.br/ws/%s/json/", numero);
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForEntity(url, String.class);
	}

}
